angular.module('testapp', [
	'ngRoute',
]).
config(function($routeProvider){
$routeProvider
  .when('/', {
  	templateUrl: 'views/home.html',
  	controller: 'homeCtrl',
  })
  .when('/register', {
    templateUrl: 'views/register.html',
    controller: 'registerCtrl',
  })
  .otherwise({
    redirectTo: '/'
  });
}).
controller('homeCtrl', function($log){
  $log.debug('Welcome to the testapp!');
}).
controller('registerCtrl', function($scope, $log){
   $scope.submit = function(user){
      if(user.email === user.email2 && user.password === user.password2 && validatePNum(user.ssn) == true){
         $log.debug('submit: user = ', user);
         alert("Registration successful");
      }
      else{
         alert("Registration unsuccessful. Please make sure that emails and passwords match, and that the social security number is correct.");
      }		  
   };
});

function validatePNum(sPNum)
{
  var numbers = sPNum.match(/^(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)$/);
  var checkSum = 0;

  var d = new Date();
  if (!isDate(sPNum.substring(0,4),sPNum.substring(4,6),sPNum.substring(6,8))) {
    //Om datumet inte är korrekt så returneras false
    return false;
  }
  //returnera false om numbers är null
  if (numbers == null) { return false; }
  // nedan sker en galen beräkning enligt luhn...phew
  var n;
  for (var i = 3; i <= 12; i++)
  {
    n=parseInt(numbers[i]);
    if (i % 2 == 0) {
      checkSum+=n;
    } else {
      checkSum+=(n*2)%9+Math.floor(n/9)*9
    }
  }

  if (checkSum%10==0) { return true;}
  return false;
}

function getYear(y) { return (y < 1000) ? y + 1900 : y; }

function isDate(year, month, day){
  month = month - 1; //räknas som 0-11
  var tmpDate = new Date(year,month,day);
  if ((getYear(tmpDate.getYear()) == year) && (month == tmpDate.getMonth()) && (day == tmpDate.getDate()))
    return true;
  else
    return false;
}